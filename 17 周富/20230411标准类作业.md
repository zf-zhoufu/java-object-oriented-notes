# 笔记

## 类和对象

面向过程：是一种以**过程**为中心的编程思想，实现功能的每一步，都是自己实现的

面向对象：是一种以**对象**为中心的编程思想，通过指挥对象实现具体的功能

类是对现实生活中一类具有**共同属性**和**行为**的事物的抽象

### 类的组成

##### 属性：该事物的各种特征

例如大学生事物的属性：姓名、年龄、毕业院校…

##### 行为：该事物存在的功能（能够做的事情）

例如大学生事物行为：学习、Java编程开发

### **类和对象的关系**

**类**：类是对现实生活中一类具有共同属性和行为的事物的抽象

**对象**：是能够看得到摸的着的真实存在的实体

### **类的定义**

类的组成：**属性**和**行为**

属性：在类中通过**成员变量**来体现（类中方法外的变量）

行为：在类中通过**成员方法**来体现（和前面的方法相比去掉static关键字即可）

```
public class 类名 {
         // 成员变量
          	变量1的数据类型 变量1；
          	变量2的数据类型 变量2;
          	…
		// 成员方法
          	方法1;
          	方法2;		
}

/*
类的定义步骤：
定义类
编写类的成员变量
编写类的成员方法
/*
```

### 对象的使用

#### 创建对象

```
格式：类名 对象名 = new 类名();
范例：Student s = new Student();
```

#### 使用对象

```
1：使用成员变量
	格式：对象名.变量名
	范例：p.name
2：使用成员方法
	格式：对象名.方法名()
	范例：p.study();
```

### **垃圾回收**

当堆内存中，**对象**或**数组**产生的地址，通过任何方式都不能被找到后，就会被判定为内存中的**“垃圾”**

垃圾会被Java垃圾回收器，空闲的时候自动进行清理

### **成员变量和局部变量**

成员变量：类中方法外的变量

局部变量：方法中的变量

~~~java

import java.security.SecureRandom;

public class Employee {
//    包含属性：编号、姓名、年龄、薪资，
    private int id;
    private String name;
    private int age;
    private int srlry;
    public void set(String name,int id,int age,int srlry){
        this.srlry = srlry;
        this.id = id;
        this.name=name;
        this.age = age;
    }


    public Employee(){

    }
    public Employee(int id ,String name,int age,int srlry){
        this.srlry = srlry;
        this.id = id;
        this.name=name;
        this.age = age;
    }
    public String toString(){
       return "编号"+id+"姓名"+name+"年龄"+age+"薪资"+srlry;
    }
}

~~~

~~~java
public class D1 {
//    声明员工类Employee,包含属性：编号、姓名、年龄、薪资，
//
//    在测试类的main方法中，创建2个员工对象，并为属性赋值，并打印两个员工的信息。
public static void main(String[] args) {
    Employee e = new Employee();
    e.set("张三",1,39,10000);
    System.out.println("e = " + e);

     Employee e2 = new Employee(2,"李四",39,10001);
    System.out.println("e2 = " + e2);
}

}

~~~





